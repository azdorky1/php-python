import os

class isNullError(Exception): pass

ModeAvailable = ["Penjumlahan"]

def isNull(variabel):
    if variabel is None:    
        raise isNullError
    elif not variabel:
        raise isNullError

def penjumalahan(x1,x2):
    return int(x1)+int(x2)

def modeCalc():
    print("Mode yang tersedia")
    counter = 1
    for mode in ModeAvailable:
        print(counter,". ",mode)
        counter = ++counter
    print()

def ScreenWelcome():
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Ini adalah kalkulator")
    modeCalc()
    triggerButton = input("Mode apa yang anda ingin gunakan yaitu : ")
    print(triggerButton)
    if (triggerButton is None) or (not triggerButton):
        return 0
    else:
        return int(triggerButton)

def ScreenCalc():
    return "Menu"

if __name__ == "__main__": 
    try:
        while(True):
            IntTriggerButton = ScreenWelcome()
            print()

            if (IntTriggerButton <= 0) : 
                print("Valid Number")
                print("Enter to Try Again!")
                IntTriggerButton = 99
            elif (IntTriggerButton > len(ModeAvailable)):
                print("Mode Belum Tersedia")
            else:
                if (IntTriggerButton == 1):
                    ScreenCalc()

            ErrorStopNotif = input()
    except isNullError:
        print("Variabel null")
        print()
    except KeyboardInterrupt:
        print()
        print("Program Close")
